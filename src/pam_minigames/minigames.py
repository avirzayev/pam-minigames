import random
import time
from pam_minigames import colors
from pam_minigames.utils.print_utils import print_section


def math_game():
    """
    Authenticates the user using a math problem. The function generates a simple math problem,
    And asks the user to answer it.

    :return (bool): True if users answer was correct, otherwise - False.
    """
    first_number = random.randint(-20, 50)
    second_number = random.randint(-20, 50)

    print_section(text="Hey There! I will authenticate you if you help me with my math homework!\n"
                       "Can you solve this problem: {first_number} + {second_number} = ?".format(
        first_number=first_number,
        second_number=second_number),
        header_text="Have you done your homework?", header_color=colors.YELLOW)

    answer = raw_input(colors.YELLOW + "\nAnswer: " + colors.ENDC)

    return answer == str(first_number + second_number)


def time_game():
    """
    Authenticates the user by asking him the time.

    :return (bool): True if users answer was correct, otherwise - False.
    """
    print_section(text="Hello There! Can you tell me what the time is? Answer quickly! The clock is ticking!",
                  header_text="WTF is the Time??", header_color=colors.YELLOW)

    answer = raw_input(colors.YELLOW + "\nAnswer (hh:mm format): " + colors.ENDC)
    current_time = time.localtime()

    if current_time.tm_min < 10:
        correct_answer = str(current_time.tm_hour) + ":0" + str(current_time.tm_min)
    else:
        correct_answer = str(current_time.tm_hour) + ":" + str(current_time.tm_min)

    return answer == correct_answer


def palindrome_game():
    """
    Authenticates the user by asking to enter a palindrome word.

    :return (bool): True if users answer was correct, otherwise - False.
    """
    print_section(text="You are Lucky! I will accept every password that is a palindrome!",
                  header_text="Pa.. Pa.. Palindrome", header_color=colors.YELLOW)

    answer = raw_input(colors.YELLOW + "\nAnswer: " + colors.ENDC)

    return answer == answer[::-1]
