from setuptools import setup, find_packages

setup(
    name="pam_minigames",
    version="0.1",
    author="Avi Rzayev",
    packages=find_packages(),
    install_requires=["pathlib"]
)
