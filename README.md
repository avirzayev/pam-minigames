# Pam Minigames
This package provides a simple example of managing user authentication using PAM.

## Installation

### Step 1:
Install pam_python module from this link:
https://pam-python.sourceforge.net/

### Step 2:
Clone this repository into a local directory.

### Step 3:

Edit /etc/pam.d/common-auth and add this line to the top of the file:
```commandline
auth	[success=done user_unknown=ignore auth_err=die]	pam_python.so <full_clone_path>/src/pam_minigames/login.py
```
Edit /etc/pam.d/common-session and add this line to the top of the file:
```commandline
session	sufficient pam_python.so <full_clone_path>/src/pam_minigames/login.py
```

### Step 4
Create a group named **_fungames_** and add users to it.
```commandline
groupadd fungames
usermod -a -G fungames <testuser>
```

## Testing
You can test the module by switching users:
```commandline
su <testuser>
```
